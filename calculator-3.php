#!/usr/bin/php
<?php

//Addition (+)
function add($x, $y) {
	return $x+$y;
}

//Subtraction (-)
function substract($x, $y) {
	return $x-$y;
}

//Multiplication (x) 
function multiply($x, $y) {
	return $x*$y;
}

//Division (/)
function divide($x, $y) {
	return $x/$y;
}

//Modulus (%)
function mudulus($x, $y) {
	return $x%$y;
}

function validate($formular) {

	if(count($formular) > 0) {
		foreach ($formular as $key => $value) {
			//validate symbols
			if($key%2 === 1 && !($value === 'x' || $value === '/' || $value === '%' || $value === '+' || $value === '-')) {
				echo 'Error: Syntax Error'."\n";
				exit;
			} else if ($key%2 === 0 && (string)($value) !== '0' && abs($value) === 0) {
				//validate numbers
				echo 'Error: Syntax Error'."\n";
				exit;
			} else if ($value === '/' && $formular[$key+1] === '0' || $value === '%' && $formular[$key+1] === '0' ) {
				//validate division
				echo 'Error: Division by zero'."\n";
				exit;
			}
		}
	} else {
		echo 'Error: No Input'."\n";
		exit;
	}
}

//Find indexes of higher precedence
function findHigherPrecedence($formular) {
	$higher = array();
	
	foreach ($formular as $key => $value) {
		if($value === 'x' || $value === '/' || $value === '%') {
			$higher[] = $key;
		}
	}
	return $higher;
}

//Find indexes of lower precedence
function findLowerPrecedence($formular) {
	$lower = array();

	foreach ($formular as $key => $value) {
		if($value === '+' || $value === '-') {
			$lower[] = $key;
		}
	}
	return $lower;
}

//Make new formular that gets rid of the elements which finish calculation.  
function moveIndex($start, $formular) {
	$tempFormular = array();

	//Keep the elements before changed element.
	for($i=0; $i<$start-2; $i++) {
		$tempFormular[$i] = $formular[$i];
	}

	//re-order elements after changed element.
	foreach ($formular as $key => $value) {
		if($key >= $start) {
			$tempFormular[] = $formular[$key];
			$i++;
		}
	}

	return $tempFormular;
}

//Implement all higher precedence calculations
function calculateHigher($formular) {
	//Get all indexes of all higer precedences first time.
	$higher = findHigherPrecedence($formular);

	while(count($higher) > 0  && count($formular) > 1) {

				switch ($formular[$higher[0]]) {
						//Put result in the element before high precedence
						case 'x':
							$formular[$higher[0]-1] = multiply($formular[$higher[0]-1], $formular[$higher[0]+1]);
							break;
						case '/':
							$formular[($higher[0]-1)] = divide($formular[$higher[0]-1], $formular[$higher[0]+1]);
							break;
						case '%':
							$formular[($higher[0]-1)] = mudulus($formular[$higher[0]-1], $formular[$higher[0]+1]);
							break;
				}
				//re-order the formular array because of the calculation
				$formular = moveIndex($higher[0]+2, $formular);
				//get the latest high precedences again until there is none
				$higher = findHigherPrecedence($formular);

	}
	return $formular;
}

//Implement all lower precedence calculations
function calculateLower($formular) {
	$lower = findLowerPrecedence($formular);
	//Similar to calculateHigher method, only symbols are handled differently.
	while(count($lower) > 0  && count($formular) > 1) {

				switch ($formular[$lower[0]]) {
						case '+':
							$formular[$lower[0]-1] = add($formular[$lower[0]-1], $formular[$lower[0]+1]);
							break;
						case '-':
							$formular[$lower[0]-1] = substract($formular[$lower[0]-1], $formular[$lower[0]+1]);
							break;
				}
				$formular = moveIndex($lower[0]+2, $formular);
				$lower = findLowerPrecedence($formular);
	}
	return $formular;
}

$formular = array();

foreach ($argv as $key => $value) {
	if($key != 0) {
		$formular[$key-1] = $value;
	}
}

validate($formular);
$formular = calculateHigher($formular);
$formular = calculateLower($formular);

if(count($formular)===1) echo ceil($formular[0])."\n";

?>