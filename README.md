# Coding Tests from company N
# Finished by: John Yao

## Task Requirements


1. Not allowed to use built in functions but Math libraries and string to number parser where needed.
2. Input should be taken from the terminal and then output the result back to the screen
3. Finish in 2 days

## Tasks Descriptions
1. Given an input with a string, use recursion to find the first position letter a is on.

Input #1:

bcdefgabcdefg

Output #1:

Position: 7



Input #2:

bcdefgbcdefg

Output #2:

Position: N/A



2. Write a program where, given a number of random string, it will output the the calculated result as a report. The equal signs used for the report title also needs to be printed out. See the example below.



Input #1:

a_++efg



Output #1:

=========

==Report==

a: 1

_: 1

+: 2

e: 1

f: 1

g: 1

Total Characters: 7



3. Write a calculator which takes in a number of string input and perform calculation. The input can accept a number of operators. The operators are:

Addition (+)

Subtraction (-)

Multiplication (x)

Division (/)

Modulus (%)



The multiplication, division, and modulus have a higher precedence over the addition and multiplication rule. Numbers can optionally have the negative (-) sign in front of them. Decimal of all individual calculation are kept, and can only be rounded down as the total calculation. Below are sample input and output



Input #1:

3 x 3 + 3 - 2

Output #1:

10



Input #2:

3 x 3 + 3 - -2

Output #2:

14



Input #3:

1 ++ 2 x 2 x / 4 + 0

Output #3:

Error: Syntax Error



Input #4:

2 x 3 + 2 + 5 + 7 - 55 - 0 - 12 - 4 / 17 / 3 x 7 + 2 - 16

Output #4:

-61



Input #5:

1 + 1 - 1 x 2 / 0

Output #5:

Error: Division by zero



Input #6:

1 % 3 - 1

Output #6:

0
