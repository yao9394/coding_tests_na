#!/usr/bin/php
<?php

//Get the length of a string
function getStrLen($s)
{
	$i=0;

	while ($s[$i] != '' && $s[$i] != ' ') {
	  $i++;
	}

	return $i;
}

//Convert string to an array
function convertToArray($s, $length) {
	$charArray = array();

	for($i=0; $i<$length; $i++) {
		$charArray[$i] = $s[$i];
	}

	return $charArray;
}

//Check if a char already exists
function checkChar($char, $currentLength, $inArray) {
	for($i=0; $i<$currentLength; $i++) {
		if($inArray[$i] == $char) {
			return true;
		}
	}
	return false;
}

function printReport($charFigures) {
	$total = 0;
	echo '========='."\n";
	echo '==Report=='."\n";
	foreach ($charFigures as $key => $value) {
		$total += $value;
		echo $key.': '.$value."\n";
	}
	echo 'Total Characters: '.$total."\n";

}

//Make sure there is only one argument allowed.
if(count($argv) != 2) {
	echo 'Invalid inputs'."\n"; exit;
} else {

	//convert string to array
	//put a space after the string for marking end of string
	$charArray = convertToArray($argv[1],getStrLen($argv[1].' '));

	//array to store existing chars
	$foundChars = array();

	//array for the figures
	$charFigures = array();

	//index for found chars array
	$currentLength = 0;

	//loop through the array
	foreach ($charArray as $key => $char) {
		if($key > 0) {
			if(checkChar($char, $currentLength, $foundChars)) {
				//char already exists
				$charFigures[$char]++;
			} else {
				//new char found
				$foundChars[$currentLength] = $char;
				$charFigures[$char] = 1;
				$currentLength++;
			}
		} else {
			//first char in the string
			$foundChars[$key] = $char;
			$charFigures[$char] = 1;
			$currentLength = 1;
		}
	}

	printReport($charFigures);

}


?>