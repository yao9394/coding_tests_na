#!/usr/bin/php
<?php

//Get the length of a string
function getStrLen($s)
{
	$i=0;

	while ($s[$i] != '' && $s[$i] != ' ') {
	  $i++;
	}

	return $i;
}

//Convert string to an array
function convertToArray($s, $length) {
	$charArray = array();

	for($i=0; $i<$length; $i++) {
		$charArray[$i] = $s[$i];
	}

	return $charArray;
}

//Make sure there is only one argument allowed.
if(count($argv) != 2) {
	echo 'Invalid inputs'."\n"; exit;
} else {

	//convert string to array
	//put a space after the string for marking end of string
	$charArray = convertToArray($argv[1],getStrLen($argv[1].' '));

	//loop through the array and find 'a'
	foreach ($charArray as $key => $char) {
		if($char === 'a') {
			echo 'Position: '.($key+1)."\n"; 
			exit;
		}
	}
	echo 'Position: N/A'."\n";
}


?>